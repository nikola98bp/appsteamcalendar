window.onload = function()
{
    var usr = new User();
    this.console.log('onload');
    usr.setEventsHandlers();
}


function User()
{

    this.setEventsHandlers = function()
    {
        var primenibtn = document.getElementById("primeni");
        primenibtn.addEventListener("click",Primeni);

        //citimo tabllu
        TableClear();
    }

    //bristemo tbody i opet doajemo sa istim id-jem
    function TableClear()
    {
        var parent = document.getElementById("table");
        var child = document.getElementById("tablebody");
        parent.removeChild(child);

        var newTable = document.createElement('tbody');
        newTable.id = "tablebody";

        parent.appendChild(newTable);
    }

    function Primeni()
    {
        console.log("Primeni");

        //datetime
        var datetimepickerVal = document.getElementById("datepicker").value;
        console.log(datetimepickerVal);
        
        //prostorija
        var room = document.getElementById("prostorije");
        var roomName = room.options[room.selectedIndex].value;
        console.log("room name" + roomName);

        //professor
        var professor = document.getElementById("profesori");
        var professorName = professor.options[professor.selectedIndex].value;
        console.log("prof name " + professorName);

        //smer
        var department = document.getElementById("smerovi");
        var departmentName = department.options[department.selectedIndex].value;
        console.log("dep name " + departmentName);

          
        //citanje
        //daj mi sve iz baze na butu <baza>/event
        return firebase.database().ref('/event/').once('value').then(function(snapshot) 
        {

            var parent = document.getElementById("table");
            var child = document.getElementById("tablebody");
            parent.removeChild(child);

            var newTable = document.createElement('tbody');
            newTable.id = "tablebody";

            parent.appendChild(newTable);
            

            var b = 0;
            snapshot.forEach(function(el) 
            {
                console.log(el.val());
                //izvlacimo ono sto je korisno iz el
                el = el.val();

                //ako je b 0 - odbacujemo taj event jer ne odgovara upitu
                //ako je b 1 - onda prikazujemo
                b = 1;
                if(datetimepickerVal != null && datetimepickerVal != "")
                {
                    if(datetimepickerVal != el['date'])
                        b = 0;
                }

                if(roomName != null && roomName != "")
                {
                    if(roomName != el['room'])
                        b = 0;
                }

                if(professorName != null && professorName != "")
                {
                    if(professorName != el['professor'])
                        b = 0;
                }

                if(departmentName != null && departmentName != "")
                {
                    if(departmentName != el['department'])
                        b = 0;
                }
                
                if(b === 1)
                {
                    var row = document.createElement('tr');
                    newTable.appendChild(row);
                    
                    var date = document.createElement('th');
                    date.innerHTML = el['date'];
                    row.appendChild(date);

                    var time = document.createElement('th');
                    time.innerHTML = el['timestart'] + "-" + el['timeend'];
                    row.appendChild(time);

                    var subject = document.createElement('td');
                    subject.innerHTML = el['subject'];
                    row.appendChild(subject);

                    var professor = document.createElement('td');
                    professor.innerHTML = el['professor'];
                    row.appendChild(professor);

                    var room = document.createElement('td');
                    room.innerHTML = el['room'];
                    row.appendChild(room);

                    var department = document.createElement('td');
                    department.innerHTML = el['department'];
                    row.appendChild(department);
                }
            });

        });

        
    }
};

   

