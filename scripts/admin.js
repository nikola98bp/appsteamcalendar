window.onload = function()
{
    var usr = new Admin();
    this.console.log('onload');
    usr.setEventsHandlers();
    
}

function Admin()
{
    //rezultati iz baze u obliku objekata
    this.results = [];
    //kljucevi koji odgovaraju svakom objektu iz results
    var keys = [];
    //kad korisnik odabere koji event hoce da promeni u indexForChange se cuva njegov value 
    //a njnegov value je ustvari pozicija tog objekta u results odnostno njegovog kljuca u keys
    var indexForChange = -1;

    this.setEventsHandlers = function()
    {
        var rezervisibtn = document.getElementById("rezervisi");
        rezervisibtn.addEventListener("click",Rezervisi);

        var pretrazibtn = document.getElementById('pretrazi');
        pretrazibtn.addEventListener("click",Pretrazi);

        var izadjiIzmanaBtn = document.getElementById("izadjiIzmena");
        izadjiIzmanaBtn.addEventListener("click",CloseEditMode);

        var potvrdiIzmenaBtn = document.getElementById("potvrdiizmenu");
        potvrdiIzmenaBtn.addEventListener("click",PotvrdiIzmenu);

        var parent = document.getElementById("table");
            var child = document.getElementById("tablebody");
            parent.removeChild(child);

            var newTable = document.createElement('tbody');
            newTable.id = "tablebody";

            parent.appendChild(newTable);
    }

    function Pretrazi()
    {
        console.log("Pretrazi!");
        //datetime
        var datetimepickerVal = document.getElementById("datepicker").value;
        console.log(datetimepickerVal);
        
        //prostorija
        var room = document.getElementById("prostorije");
        var roomName = room.options[room.selectedIndex].value;
        console.log("room name" + roomName);

        //professor
        var professor = document.getElementById("profesori");
        var professorName = professor.options[professor.selectedIndex].value;
        console.log("prof name " + professorName);

        //predmeti
        var subject = document.getElementById("predmeti");
        var subjectName = subject.options[subject.selectedIndex].value;
        console.log("subject name "+subjectName);

        //smer
        var department = document.getElementById("smerovi");
        var departmentName = department.options[department.selectedIndex].value;
        console.log("dep name " + departmentName);

        return firebase.database().ref('/event/').once('value').then(function(snapshot) 
        {
            var parent = document.getElementById("table");
            var child = document.getElementById("tablebody");
            parent.removeChild(child);

            var newTable = document.createElement('tbody');
            newTable.id = "tablebody";

            parent.appendChild(newTable);

            var b = 0;
            this.results = [];
            keys = [];
            //brojac koji uvecavamo za svaku iteraciju sluci samo da se njegova vrednost upise u value 
            //elementa koji oznacava izmenu i brisanje kako bi kad korisnik klikne na odgovarajuce elemente znali koji
            //objekat brisemo i koji je kljuc
            var index = 0;
            snapshot.forEach(function(el) 
            {
                console.log(el.val());
                console.log(el.ref_.path.pieces_[1]);
                
                //ovako izvalcimo kljuc iz el
                //el.ref_.path.pieces_[0] je ovde <event>
                //el.ref_.path.pieces_[1] je ovde <kljuc>
                var key = el.ref_.path.pieces_[1];

                el = el.val();

                b = 1;
                if(datetimepickerVal != null && datetimepickerVal != "")
                {
                    if(datetimepickerVal != el['date'])
                        b = 0;
                }

                if(roomName != null && roomName != "")
                {
                    if(roomName != el['room'])
                        b = 0;
                }

                if(professorName != null && professorName != "")
                {
                    if(professorName != el['professor'])
                        b = 0;
                }

                if(subjectName != null && subjectName != "")
                {
                    if(subjectName != el['subject'])
                        b = 0;
                }

                if(departmentName != null && departmentName != "")
                {
                    if(departmentName != el['department'])
                        b = 0;
                }
                
                if(b === 1)
                {
                    var row = document.createElement('tr');
                    newTable.appendChild(row);

                    var date = document.createElement('th');
                    date.innerHTML = el['date'];
                    row.appendChild(date);

                    var time = document.createElement('th');
                    time.innerHTML = el['timestart'] + "-" + el['timeend'];
                    row.appendChild(time);

                    var subject = document.createElement('td');
                    subject.innerHTML = el['subject'];
                    row.appendChild(subject);

                    var professor = document.createElement('td');
                    professor.innerHTML = el['professor'];
                    row.appendChild(professor);

                    var room = document.createElement('td');
                    room.innerHTML = el['room'];
                    row.appendChild(room);

                    var department = document.createElement('td');
                    department.innerHTML = el['department'];
                    row.appendChild(department);

                    var icons = document.createElement("td");

                    var aChange =document.createElement("a");
                    aChange.value = index;
                    aChange.onclick = function() { ShowItemForEdit(aChange.value); }
                    var iChange = document.createElement("i");
                    iChange.className = "far fa-edit";
                    aChange.appendChild(iChange);

                    var aDelete = document.createElement("a");
                    aDelete.value = index;
                    aDelete.onclick = function() { DeleteItem(aDelete.value); }
                    var iDelete = document.createElement("i");
                    iDelete.className = "fas fa-trash-alt";
                    aDelete.appendChild(iDelete);

                    icons.appendChild(aChange);
                    icons.appendChild(aDelete);

                    row.appendChild(icons);

                    //dodajemo objekat u nis
                    this.results.push(el);
                    //dodajemo kljuc
                    keys.push(key);
                    //inkrement
                    index = index + 1;
                }

                
            });

        });
    }

    function DeleteItem(itemIndex)
    {
        console.log("Delete item " + itemIndex);
        console.log("Key " + keys[itemIndex]);
        console.log(this.results[itemIndex]);

        //u prevodu objekat na putanji <baza>/event/456456 
        //456456 je npr neki kljuc setuj na  {} tj na prazan objekat
        //kad to uradimo fire base obrise objekat :D 
        firebase.database().ref('event/'+keys[itemIndex]).set({});

        Pretrazi();
    }

    function CloseEditMode()
    {
        var primenaDiv = document.getElementById("primena");
        var izmenaDiv = document.getElementById("izmena");
        primenaDiv.style.visibility = "initial";
        izmenaDiv.style.visibility = "hidden";
    }

    function ClearElements()
    {
        //datetime
        var datetimepicker = document.getElementById("datepicker");
        datetimepicker.value = "";

        //prostorija
        var room = document.getElementById("prostorije");
        room.value = "";
        

        //profesor
        var professor = document.getElementById("profesori");
        professor.value = "";

        //smer
        var department = document.getElementById("smerovi");
        department.value = "";

        //predmet
        var subject = document.getElementById("predmeti");
        subject.value = "";


        //time from to
        var od_sat_elem = document.getElementById('od_sat');
        var od_minut_elem = document.getElementById('od_minut');
        var do_sat_elem = document.getElementById('do_sat');
        var do_minut_elem = document.getElementById('do_minut'); 

        od_sat_elem.value = "";
        od_minut_elem.value = "";
        do_sat_elem.value = "";
        do_minut_elem.value = "";
    }

    function ShowItemForEdit(itemIndex)
    {
        console.log("Change item " + itemIndex);
        console.log("Key " + keys[itemIndex]);
        console.log(this.results[itemIndex]);

        //zamena opcija
        var primenaDiv = document.getElementById("primena");
        var izmenaDiv = document.getElementById("izmena");
        primenaDiv.style.visibility = "hidden";
        izmenaDiv.style.visibility = "initial";
        

        //datetime
        var datetimepicker = document.getElementById("datepicker");
        datetimepicker.value = this.results[itemIndex].date;

        //prostorija
        var room = document.getElementById("prostorije");
        room.value = this.results[itemIndex].room;
        

        //profesor
        var professor = document.getElementById("profesori");
        professor.value = this.results[itemIndex].professor;

        //smer
        var department = document.getElementById("smerovi");
        department.value = this.results[itemIndex].department;

        //predmet
        var subject = document.getElementById("predmeti");
        subject.value = this.results[itemIndex].subject;


        //time from to
        var od_sat_elem = document.getElementById('od_sat');
        var od_minut_elem = document.getElementById('od_minut');
        var do_sat_elem = document.getElementById('do_sat');
        var do_minut_elem = document.getElementById('do_minut'); 

        var od_sat = this.results[itemIndex].timestart.split(":")[0];
        var od_minut = this.results[itemIndex].timestart.split(":")[1];
        var do_sat = this.results[itemIndex].timeend.split(":")[0];
        var do_minut = this.results[itemIndex].timeend.split(":")[1];

        od_sat_elem.value = od_sat;
        od_minut_elem.value = od_minut;
        do_sat_elem.value = do_sat;
        do_minut_elem.value = do_minut;
        
        indexForChange = null;
        indexForChange = itemIndex;
    }
    
    function Rezervisi()
    {
        console.log("Primeni");

        //datetime
        var datetimepickerVal = document.getElementById("datepicker").value;
        console.log(datetimepickerVal);
        
        //prostorija
        var room = document.getElementById("prostorije");
        var roomName = room.options[room.selectedIndex].value;
        console.log("room name" + roomName);

        //professor
        var professor = document.getElementById("profesori");
        var professorName = professor.options[professor.selectedIndex].value;
        console.log("prof name " + professorName);

        //predmet
        var subject = document.getElementById('predmeti');
        var subjectName = subject.options[subject.selectedIndex].value;
        console.log("subject name " + subjectName);

        //smer
        var department = document.getElementById("smerovi");
        var departmentName = department.options[department.selectedIndex].value;
        console.log("dep name " + departmentName);

        var od_sat = document.getElementById('od_sat');
        var od_minut = document.getElementById('od_minut');
        var do_sat = document.getElementById('do_sat');
        var do_minut = document.getElementById('do_minut'); 

        var od_ = od_sat.value + ":" + od_minut.value;
        var do_ = do_sat.value + ":" + do_minut.value;

        console.log('Od ' + od_);
        console.log('Do ' + do_);

        if(datetimepickerVal == "" || roomName == "" || professorName == "" || departmentName == "" || subjectName == ""
            || od_sat == "" || od_minut == "" || do_sat == "" || do_minut == "")
            {
                alert("Popunite sva polja!");
                return;
            }
            

        //pravimo id tako sto neki radnom pretvorimo u int tj zaokruzimo
        //resenje nije najsrecnije jer je teoretski moguce da se poklope vrednosti dva puta pa da se objekat prepise 
        //ali sta sad da se radi :D
        var id = Math.floor(Math.random() * 10000000);
        firebase.database().ref('event/'+id).set({
            date: datetimepickerVal,
            department: departmentName,
            professor: professorName,
            room: roomName,
            subject: subjectName,
            timeend: do_,
            timestart: od_
        });


        ClearElements();
        Pretrazi();
    }

    function PotvrdiIzmenu()
    {
        if(indexForChange === -1)
        {
            alert("Error!");
            return;

        }

        console.log("index for change: " + indexForChange);
        

        //datetime
        var datetimepickerVal = document.getElementById("datepicker").value;
        console.log(datetimepickerVal);
        
        //prostorija
        var room = document.getElementById("prostorije");
        var roomName = room.options[room.selectedIndex].value;
        console.log("room name" + roomName);

        //professor
        var professor = document.getElementById("profesori");
        var professorName = professor.options[professor.selectedIndex].value;
        console.log("prof name " + professorName);

        //predmet
        var subject = document.getElementById('predmeti');
        var subjectName = subject.options[subject.selectedIndex].value;
        console.log("subject name " + subjectName);

        //smer
        var department = document.getElementById("smerovi");
        var departmentName = department.options[department.selectedIndex].value;
        console.log("dep name " + departmentName);

        var od_sat = document.getElementById('od_sat');
        var od_minut = document.getElementById('od_minut');
        var do_sat = document.getElementById('do_sat');
        var do_minut = document.getElementById('do_minut'); 

        var od_ = od_sat.value + ":" + od_minut.value;
        var do_ = do_sat.value + ":" + do_minut.value;


        firebase.database().ref('event/'+keys[indexForChange]).set({
            date: datetimepickerVal,
            department: departmentName,
            professor: professorName,
            room: roomName,
            subject: subjectName,
            timeend: do_,
            timestart: od_
        });

        CloseEditMode();
        ClearElements();
        Pretrazi();

    }
}